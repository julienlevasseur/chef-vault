# # encoding: utf-8

# Inspec test for recipe terraform::default

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

require 'json'
nodefile = '/tmp/kitchen/nodes/node.json'
node = json(nodefile).params if File.exist?(nodefile) || json('/tmp/kitchen/nodes/node.json').params

describe file("#{node['default']['vault']['install_dir']}/vault") do
  it { should exist }
end

describe command("#{node['default']['vault']['install_dir']}/vault -v|head -1") do
  its('stdout') { should match node['default']['vault']['version'] }
end
