use_inline_resources

def whyrun_supported
  true
end

def vault_is_installed
  return ::File.file?("#{new_resource.install_dir}/vault")
end

def vault_installed_version
  require 'open3'
  stdout, stderr, status = Open3.capture3(
    "#{new_resource.install_dir}/vault -v|head -1|awk '{print $2}'|sed -e 's/v//g'"
  )[0]
  return stdout.sub!("\n", "")
end

def cleanup
  ::File.delete("#{new_resource.install_dir}/vault")
end

def download_archive
  if new_resource.platform == 'ubuntu' || new_resource.platform == 'centos'
    remote_file "/tmp/vault_#{new_resource.version}_linux_amd64.zip" do
      source "https://releases.hashicorp.com/vault/#{new_resource.version}/vault_#{new_resource.version}_linux_amd64.zip"
      mode '0644'
      action :create
    end
  elsif new_resource.platform == 'mac_os_x'
    remote_file "/tmp/vault_#{new_resource.version}_darwin_amd64.zip" do
      source "https://releases.hashicorp.com/vault/#{new_resource.version}/vault_#{new_resource.version}_darwin_amd64.zip"
      mode '0644'
      action :create
    end
  end
end

def unzip_archive
  zipfile "/tmp/vault_#{new_resource.version}_linux_amd64.zip" do
    into new_resource.install_dir
  end
end

action :install do
  Chef::Log.info("[DEBUG] in :install")
  unless vault_is_installed
    converge_by "Installing Vault '#{new_resource.version}'" do
      begin
        unless Dir.exist?(new_resource.install_dir)
          ::Chef.Log.info("[DEBUG] create install_dir")
          require 'fileutils'
          FileUtils.mkdir_p "#{new_resource.install_dir}"
        end
    
        download_archive
        unzip_archive
        Chef::Log.debug("Installing Vault #{new_resource.version}")
      rescue
        Chef::Log.error("Failed to install Vault #{new_resource.version}")
      end
    end
  else
    if vault_installed_version < new_resource.version
      converge_by "Upgrading Vault to '#{new_resource.version}'" do
        begin
          cleanup
          download_archive
          unzip_archive
          Chef::Log.debug("Upgrading Vault version: from #{vault_installed_version} to #{new_resource.version}")
        rescue
          Chef::Log.error("Failed to update Vault from #{vault_installed_version} to #{new_resource.version}")
        end
      end
    end
  end
end

action :uninstall do
  if vault_is_installed
    converge_by "Uninstalling Vault" do
      begin
        cleanup
        Chef::Log.debug("Uninstalling Vault")
      rescue
        Chef::Log.error("Failed to uninstall Vault")
      end
    end
  end
end