name 'vault'
maintainer 'Julien Levasseur'
maintainer_email 'contact@julienlevasseur.ca'
license 'MIT'
description 'Installs/Configures vault'
long_description 'Installs/Configures vault'
version '0.1.0'
chef_version '>= 12.14' if respond_to?(:chef_version)
issues_url 'https://gitlab.com/julienlevasseur/chef-vault/issues'
source_url 'https://gitlab.com/julienlevasseur/chef-vault'

depends 'zipfile'

supports 'ubuntu'
supports 'centos'
supports 'debian'
