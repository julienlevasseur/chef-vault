
default['vault']['version'] = '1.3.2'

default['vault']['url'] = 'http://localhost:8200'

default['vault']['install_dir'] = '/usr/local/bin'
default['vault']['config_dir']  = '/etc/vault'
default['vault']['data_dir']    = '/var/lib/vault'
default['vault']['log_dir']     = '/var/log/vault'
default['vault']['bind_addr']   = '0.0.0.0'

if node['platform'] == 'ubuntu'
  default['vault']['owner'] = 'root'
  default['vault']['group'] = 'root'
elsif node['platform'] == 'mac_os_x'
  default['vault']['owner'] = 'root'
  default['vault']['group'] = 'sys'
end

default['vault']['config'] = {}

default['vault']['datacenter'] = nil

# default['vault']['service']['ExecStart'] = "#{node['vault']['install_dir']}/vault agent -server -client=0.0.0.0 -bootstrap-expect=1 -ui -dev -config-dir #{node['vault']['config_dir']}"
default['vault']['service']['ExecStart'] = "#{node['vault']['install_dir']}/vault server -dev"
