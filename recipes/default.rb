#
# Cookbook:: vault
# Recipe:: default
#

directory node['vault']['config_dir'] do
  recursive true
end

vault node['vault']['version']

pretty_settings = Chef::JSONCompat.to_json(node['vault']['config'])

template "#{node['vault']['config_dir']}/config.json" do
  source 'config.json.erb'
  owner 'root'
  group 'root'
  mode '0644'
  variables settings: pretty_settings
end

if ::File.exists?('/bin/systemd')
  template '/lib/systemd/system/vault.service' do
    source 'vault.service.erb'
    owner 'root'
    group 'root'
    mode '0644'
  end

  service 'vault' do
    action [:enable, :start]
    not_if { Chef::Config[:file_cache_path].include?('kitchen') }
  end
else
  template '/etc/init.d/vault' do
    source 'vault-init.erb'
    owner 'root'
    group 'root'
    mode '0755'
  end

  execute 'enable-vault-service' do
    command '/etc/init.d/vault start'
    not_if 'ps -aef|grep vault|grep -v grep'
    not_if { Chef::Config[:file_cache_path].include?('kitchen') }
  end
end

execute 'enable-vault-service' do
  command "#{node['vault']['service']['ExecStart']} &"
  not_if 'ps -aef|grep vault|grep -v grep'
  only_if { Chef::Config[:file_cache_path].include?('kitchen') }
end

if Chef::Config[:file_cache_path].include?('kitchen')
  node.save # ~FC075
  file '/tmp/kitchen/nodes/node.json' do
    owner 'root'
    group 'root'
    mode 0755
    content ::File.open("/tmp/kitchen/nodes/#{node.name}.json").read
    action :create
    only_if { ::File.exist?("/tmp/kitchen/nodes/#{node.name}.json") }
  end
end
